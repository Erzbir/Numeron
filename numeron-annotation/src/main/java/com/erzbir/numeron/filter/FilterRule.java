package com.erzbir.numeron.filter;

/**
 * @author Erzbir
 * @Date: 2022/11/19 10:54
 * <p>规则过滤类型</p>
 */
public enum FilterRule {
    BLACK,
    NORMAL,
    NONE
}
